---
layout: post
category : meta
tags : [intro, helloWorld]
---
{% include JB/setup %}
This is a test post from Jekyll on GitHub Pages. This seemed like a fun way to use an extra 
allotted repository and I feel priviledged to be able to host Web content from a GitHub 
subdomain. I plan to post both new and previously created snippets and provide a bit of 
commentary in case anyone else finds their way here through the catacombs of Google. A lot of 
what I've been working with lately has involved Java, the Android platform, CouchDB, SQL, 
NodeJS, and various WebSocket libraries (most recently SockJS). I'm sure a few things about 
each will end up making their way here, so feel free to subscribe to [the RSS feed](/atom.xml) 
if those topics might interest you.